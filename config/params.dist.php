<?php

return [
    'adminEmail' => 'admin@example.com',
    'user.passwordResetTokenExpire' => 3600*24, //3600 miliseconds * 24 hours
];
